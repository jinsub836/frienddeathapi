package com.js.frienddeathapi.controller;

import com.js.frienddeathapi.model.friend.*;
import com.js.frienddeathapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {

    private final FriendService friendService;

    @PostMapping("/new")
    public String setFriend(@RequestBody FriendRequest request){
        friendService.setFriend(request);
        return "입력 완료";
    }

    @GetMapping("/all")
    public List<FriendItem> getFriends(){return friendService.getFriends();}

    @GetMapping("/detail/{id}")
    public FriendResponse getFriend(@PathVariable long id){ return friendService.getFriend(id);}
    @PutMapping("/change/{id}")
        public String putFriend(@PathVariable long id ,@RequestBody FriendInfoChangeRequest request){
        friendService.putFriend(id, request);
        return "수정 완료";
        }
    @PutMapping("/fiend-cut/{id}")
    public String putFriendCut(@PathVariable long id , @RequestBody FriendUpdateCutReason reason){
        friendService.putFriendCut(id, reason);
        return "손절 완료";
    }
    @PutMapping("/return/{id}")
    public String putReturn(@PathVariable long id ){friendService.putReturn(id);
        return " 회복 완료";}
    }