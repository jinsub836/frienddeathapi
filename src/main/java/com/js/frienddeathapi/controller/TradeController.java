package com.js.frienddeathapi.controller;

import com.js.frienddeathapi.entity.Friend;
import com.js.frienddeathapi.model.trade.*;
import com.js.frienddeathapi.service.FriendService;
import com.js.frienddeathapi.service.TradeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/trade")
public class TradeController {
    private final TradeService tradeService;
    private final FriendService friendService;

    @PostMapping("/new/fiend-id/{fiendsId}")
    public String setTrade(long fiendsId , TradeRequest request){
        Friend friend = friendService.getData(fiendsId);
        tradeService.setTrade(friend , request);
        return "입력 완료";
    }

    @GetMapping("/all")
    public List<TradeItem> getTrades(){return tradeService.getTrades();}

    @GetMapping("/detail/{id}")
    public TradeResponse getTrade(@PathVariable long id){
        return tradeService.getTrade(id);
    }

    @GetMapping("/compare")
    public List<TradeCompare> getTradeCompare(){
        return tradeService.getTradeCompare();
    }

    @PutMapping("/change/{id}")
    public String putTrade(@PathVariable long id, @RequestBody TradeChangeRequest request){
        tradeService.putTrade(id, request);
        return "수정 완료";
    }
}
