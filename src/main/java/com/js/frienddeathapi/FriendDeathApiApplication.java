package com.js.frienddeathapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriendDeathApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FriendDeathApiApplication.class, args);
	}

}
