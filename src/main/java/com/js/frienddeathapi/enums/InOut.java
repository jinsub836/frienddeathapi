package com.js.frienddeathapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum InOut {
    IN("받음",1)
    ,OUT("줌",-1);

    private final String name;
    private final Integer number;}
