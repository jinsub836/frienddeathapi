package com.js.frienddeathapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendResponse {
    private Long id;

    private String friendName;

    private LocalDate birthday;

    private String phoneNumber;

    private String etcMemo;

}
