package com.js.frienddeathapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class FriendInfoChangeRequest {

    private String friendName;

    private LocalDate birthday;

    private String phoneNumber;

}
