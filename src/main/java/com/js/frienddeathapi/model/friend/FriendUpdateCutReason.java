package com.js.frienddeathapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FriendUpdateCutReason {
    private String cutReason;
}
