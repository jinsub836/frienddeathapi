package com.js.frienddeathapi.model.friend;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendRequest {

    private String friendName;

    private LocalDate birthday;

    private String phoneNumber;

    private String etcMemo;


}
