package com.js.frienddeathapi.model.trade;

import com.js.frienddeathapi.enums.InOut;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TradeItem {

    private Long id;

    private Long friendId;

    private String friendName;

    private String name;

    private String category;

    private Double money;

    private LocalDate tradeDate;


}
