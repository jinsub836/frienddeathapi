package com.js.frienddeathapi.model.trade;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TradeCompare {
    private String friendName;
    private Integer tradeCompare;
}
