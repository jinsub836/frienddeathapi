package com.js.frienddeathapi.model.trade;

import com.js.frienddeathapi.entity.Friend;
import com.js.frienddeathapi.enums.InOut;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TradeRequest {

    @Enumerated(value = EnumType.STRING)
    private InOut inOut;

    private String category;

    private Double money;

    private LocalDate tradeDate;
}
