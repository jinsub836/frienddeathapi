package com.js.frienddeathapi.service;

import com.js.frienddeathapi.entity.Friend;
import com.js.frienddeathapi.model.friend.*;
import com.js.frienddeathapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long friendId){ return friendRepository.findById(friendId).orElseThrow();}

    public void setFriend(FriendRequest request){
        Friend addData = new Friend();
        addData.setFriendName(request.getFriendName());
        addData.setBirthday(request.getBirthday());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCut(false);

        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends(){
        List<Friend> originlist = friendRepository.findAll();
        List<FriendItem> result = new LinkedList<>();

        for(Friend friend: originlist){
            FriendItem addItem = new FriendItem();
            addItem.setId(friend.getId());
            addItem.setFriendName(friend.getFriendName());
            addItem.setBirthday(friend.getBirthday());

            result.add(addItem);
        } return result;
    }

    public FriendResponse getFriend(long id){
        Friend originData = friendRepository.findById(id).orElseThrow();
        FriendResponse response = new FriendResponse();

        response.setId(originData.getId());
        response.setFriendName(originData.getFriendName());
        response.setBirthday(originData.getBirthday());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());


        return response;
    }

    public void putFriend(long id , FriendInfoChangeRequest request){
        Friend friend = friendRepository.findById(id).orElseThrow();

        friend.setFriendName(request.getFriendName());
        friend.setBirthday(request.getBirthday());
        friend.setPhoneNumber(request.getPhoneNumber());

        friendRepository.save(friend);
    }

    public void putFriendCut(long id , FriendUpdateCutReason reason){
        Friend originData = friendRepository.findById(id).orElseThrow();

        originData.setIsCut(true);
        originData.setCutDate(LocalDate.now());
        originData.setCutReason(reason.getCutReason());

        friendRepository.save(originData);
    }

    public void putReturn(long id){
        Friend originData = friendRepository.findById(id).orElseThrow();

        originData.setIsCut(false);
        originData.setCutDate(null);
        originData.setCutReason(null);

        friendRepository.save(originData);}
}
