package com.js.frienddeathapi.service;

import com.js.frienddeathapi.entity.Friend;
import com.js.frienddeathapi.entity.Trade;
import com.js.frienddeathapi.model.trade.*;
import com.js.frienddeathapi.repository.TradeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TradeService {
    private final TradeRepository tradeRepository;

    public void setTrade (Friend friend, TradeRequest request){
        Trade addData = new Trade();
        addData.setFriendId(friend);
        addData.setInOut(request.getInOut());
        addData.setCategory(request.getCategory());
        addData.setMoney(request.getMoney());
        addData.setTradeDate(request.getTradeDate());

        tradeRepository.save(addData);
    }

    public List<TradeItem> getTrades(){
        List<Trade> originlist = tradeRepository.findAll();
        List<TradeItem> result = new LinkedList<>();

        for (Trade trade : originlist ){
            TradeItem addItem = new TradeItem();

            addItem.setId(trade.getId());
            addItem.setFriendId(trade.getFriendId().getId());
            addItem.setFriendName(trade.getFriendId().getFriendName());
            addItem.setName(trade.getInOut().name());
            addItem.setMoney(trade.getMoney());
            addItem.setCategory(trade.getCategory());
            addItem.setTradeDate(trade.getTradeDate());

            result.add(addItem);
        } return result;
    }


    public TradeResponse getTrade(long id){
        Trade originData = tradeRepository.findById(id).orElseThrow();
        TradeResponse response = new TradeResponse();

        response.setId(originData.getId());
        response.setFriendId(originData.getFriendId().getId());
        response.setFriendName(originData.getFriendId().getFriendName());
        response.setName(originData.getInOut().name());
        response.setCategory(originData.getCategory());
        response.setMoney(originData.getMoney());
        response.setTradeDate(originData.getTradeDate());
        response.setEtcMemo(originData.getFriendId().getEtcMemo());

        return response;
    }

    public void putTrade(long id , TradeChangeRequest request){
        Trade trade = tradeRepository.findById(id).orElseThrow();

        trade.setTradeDate(request.getTradeDate());

        tradeRepository.save(trade);
    }

    public List<TradeCompare> getTradeCompare(){
        List<Trade> originlist = tradeRepository.findAll();
        List<TradeCompare> result = new LinkedList<>();
        int tradeNumber = 0;
        for(Trade trade : originlist){
            TradeCompare addItem = new TradeCompare();
            //만약 getFriendId가 같다면 , InOut 끼리 더함
            if (trade.getFriendId().equals(trade.getFriendId())){
                tradeNumber += trade.getInOut().getNumber();}
            if (tradeNumber < -4){
                addItem.setTradeCompare(tradeNumber);
                addItem.setFriendName(trade.getFriendId().getFriendName());
                result.add(addItem);}
        }return result;
    }
}
