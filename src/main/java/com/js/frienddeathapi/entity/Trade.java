package com.js.frienddeathapi.entity;

import com.js.frienddeathapi.enums.InOut;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.engine.profile.Fetch;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;

@Setter
@Getter
@Entity
public class Trade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "friendId", nullable = false)
    private Friend friendId;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private InOut inOut;

    @Column(nullable = false , length = 30)
    private String category;

    @Column(nullable = false)
    private Double money;

    @Column(nullable = false)
    private LocalDate tradeDate;

}
