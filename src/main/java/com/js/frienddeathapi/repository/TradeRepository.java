package com.js.frienddeathapi.repository;

import com.js.frienddeathapi.entity.Trade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TradeRepository extends JpaRepository<Trade , Long> {
}
