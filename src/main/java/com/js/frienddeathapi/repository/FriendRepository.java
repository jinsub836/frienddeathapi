package com.js.frienddeathapi.repository;

import com.js.frienddeathapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend , Long> {
}
